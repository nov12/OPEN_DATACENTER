#include "RedisConnector.h"


CRedisConnector::CRedisConnector()
:m_pRedisClient(NULL)
,m_iCacheMum(0)
{
	m_pRedisClient = new xRedisClient();
	m_pWRRedisList = (RedisNode *)malloc(sizeof(RedisNode)*MAX_CACHE_SIZE);
	m_pRDRedisList = (RedisNode *)malloc(sizeof(RedisNode)*MAX_CACHE_SIZE);
	memset(m_pWRRedisList, 0, sizeof(RedisNode)*MAX_CACHE_SIZE);
	memset(m_pRDRedisList, 0, sizeof(RedisNode)*MAX_CACHE_SIZE);
}


CRedisConnector::~CRedisConnector()
{
	if (m_pRedisClient)
	{
		delete m_pRedisClient;
		m_pRedisClient = NULL;
	}
}

void CRedisConnector::Start()
{
	m_pRedisClient->Init(1);
	bool bRet = m_pRedisClient->ConnectRedisCache(m_pWRRedisList, m_iCacheMum, CACHE_TYPE_WR);
	bRet = m_pRedisClient->ConnectRedisCache(m_pRDRedisList, m_iCacheMum, CACHE_TYPE_RD);
}

void CRedisConnector::Stop()
{
	m_pRedisClient->Release();
}
