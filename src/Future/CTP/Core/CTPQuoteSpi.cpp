#include "ctpImpl/CTPQuoteSpi.h"

CCTPQuoteSpi::CCTPQuoteSpi(void)
{

}


CCTPQuoteSpi::~CCTPQuoteSpi(void)
{

}

// 当客户端与交易后台建立起通信连接时（还未登录前），该方法被调用
void CCTPQuoteSpi::OnFrontConnected()
{
	m_pIQuote->OnQuoteConnected();
}

// 当客户端与交易后台通信连接断开时，该方法被调用。当发生这个情况后，API会自动重新连接，客户端可不做处理
void CCTPQuoteSpi::OnFrontDisconnected(int nReason)
{
	m_pIQuote->OnQuoteDisconnected(nReason);
}

// 心跳超时警告。当长时间未收到报文时，该方法被调用
void CCTPQuoteSpi::OnHeartBeatWarning(int nTimeLapse)
{
	OUTPUT("心跳包超时，与前置服务器断开！");
}

// 登录请求响应
void CCTPQuoteSpi::OnRspUserLogin(CThostFtdcRspUserLoginField* pRspUserLogin, CThostFtdcRspInfoField* pRspInfo, int nRequestID, bool bIsLast)
{
	m_pIQuote->OnQuoteUserLoginSuccess();
}

// 登出请求响应
void CCTPQuoteSpi::OnRspUserLogout(CThostFtdcUserLogoutField* pUserLogout, CThostFtdcRspInfoField* pRspInfo, int nRequestID, bool bIsLast)
{

}

// 错误应答
void CCTPQuoteSpi::OnRspError(CThostFtdcRspInfoField* pRspInfo, int nRequestID, bool bIsLast)
{
	OUTPUT(pRspInfo->ErrorMsg);
}

// 深度行情通知
void CCTPQuoteSpi::OnRtnDepthMarketData(CThostFtdcDepthMarketDataField* pDepthMarketData)
{
	if (pDepthMarketData == NULL) return;

	tagMarketData stMarketData;
	memset(&stMarketData, 0, sizeof(stMarketData));

	strncpy(stMarketData.szINSTRUMENT1ID, pDepthMarketData->InstrumentID, sizeof(stMarketData.szINSTRUMENT1ID));
	stMarketData.szINSTRUMENT1ID[30] = 0;

	stMarketData.dBidPrice1         = pDepthMarketData->BidPrice1;
	stMarketData.nBidVolume1        = pDepthMarketData->BidVolume1;
	stMarketData.dAskPrice1         = pDepthMarketData->AskPrice1;
	stMarketData.nAskVolume1        = pDepthMarketData->AskVolume1;

	stMarketData.dLastPrice         = pDepthMarketData->LastPrice;
	stMarketData.dAvgPrice          = pDepthMarketData->AveragePrice;
	stMarketData.nVolume            = pDepthMarketData->Volume;
	stMarketData.dOpenInt           = pDepthMarketData->OpenInterest;

	stMarketData.dUpperLimitPrice   = pDepthMarketData->UpperLimitPrice;
	stMarketData.dLowerLimitPrice   = pDepthMarketData->LowerLimitPrice;

	sprintf(stMarketData.szUpdateTime, "%s %s", pDepthMarketData->ActionDay, pDepthMarketData->UpdateTime);
	stMarketData.szUpdateTime[17] = 0;
	stMarketData.nUpdateMillisec    = pDepthMarketData->UpdateMillisec;

	m_pIQuote->OnQuoteMarketData(&stMarketData);
}

// 询价通知
void CCTPQuoteSpi::OnRtnForQuoteRsp(CThostFtdcForQuoteRspField* pForQuoteRsp)
{
	OUTPUT("OnRtnForQuoteRsp\n");
}
