
#include "ctpImpl/CTPQuoteApi.h"

#ifdef WIN32
#pragma comment(lib,"../lib/thostmduserapi.lib")
#else
#include "AutoDLL.h"
#define CREATE_QUOTE_API ("_ZN15CThostFtdcMdApi15CreateFtdcMdApiEPKcbb")
typedef CThostFtdcMdApi* (*YCreateFtdcMdApi)(const char *, const bool , const bool);
#define CTP_QUOTE_SO	"thostmduserapi.so"

#endif

//********************************************CTPQuoteApi*******************************************************//
int CCTPQuoteApi::m_nRequestSeq = 0;
CCTPQuoteApi::CCTPQuoteApi(void)
{
	m_pThostMdApi = NULL;
	m_nSessionID = 0;
}

CCTPQuoteApi::~CCTPQuoteApi(void)
{

}


// 初始化
bool CCTPQuoteApi::Init(CThostFtdcMdSpi* pThostMdSpi, char* FrontAddr)
{
		
#ifdef WIN32
	m_pThostMdApi = CThostFtdcMdApi::CreateFtdcMdApi();
#else
	YAutoDLL dll;
	YCreateFtdcMdApi _func = 0;
	_func = dll.GetFunc<YCreateFtdcMdApi>(CTP_QUOTE_SO, CREATE_QUOTE_API);
	if (!_func)
		return false;
	m_pThostMdApi = _func("",false,false);
#endif
	if (m_pThostMdApi == NULL) return false;

	m_pThostMdApi->RegisterSpi(pThostMdSpi);
	m_pThostMdApi->RegisterFront(FrontAddr);
	m_pThostMdApi->Init();
	m_pThostMdApi->Join();	//@new

	return true;
}

// 清理
void CCTPQuoteApi::Fini()
{
	if (m_pThostMdApi == NULL) return;

	m_pThostMdApi->RegisterSpi(NULL);
	m_pThostMdApi->Release();
	m_pThostMdApi = NULL;
}


// 登录
bool CCTPQuoteApi::Login(char* pszBroker, char* pszInvestor, char* pszPassword)
{
	if (m_pThostMdApi == NULL) return false;

	CThostFtdcReqUserLoginField stLoginField;
	memset(&stLoginField, 0, sizeof(stLoginField));

	strncpy(stLoginField.BrokerID, pszBroker, sizeof(stLoginField.BrokerID));
	strncpy(stLoginField.UserID, pszInvestor, sizeof(stLoginField.UserID));
	strncpy(stLoginField.Password, pszPassword, sizeof(stLoginField.Password));
	strncpy(m_szBrokerID, pszBroker, sizeof(m_szBrokerID));
	strncpy(m_szInvestorID, pszInvestor, sizeof(m_szInvestorID));

	if (m_pThostMdApi->ReqUserLogin(&stLoginField, ++m_nRequestSeq) == 0) return true;
	else return false;

}

// 断开前置
void CCTPQuoteApi::Disconnect()
{
	if (m_pThostMdApi == NULL) return;

	m_pThostMdApi->RegisterSpi(NULL);
	m_pThostMdApi->Release();
	m_pThostMdApi = NULL;

	m_nSessionID = 0;
}

// 订阅行情
bool CCTPQuoteApi::SubMarketData(char* pszInstrumentID[], int nCount)
{
	if (m_pThostMdApi == NULL) return false;

	if (m_pThostMdApi->SubscribeMarketData(pszInstrumentID, nCount) == 0) return true;
	else return false;
}
// 退订行情
bool CCTPQuoteApi::UnSubMarketData(char* pszInstrumentID[], int nCount)
{
	if (m_pThostMdApi == NULL) return false;

	if (m_pThostMdApi->UnSubscribeMarketData(pszInstrumentID, nCount) == 0) return true;
	else return false;
}

// 获取当前交易日
const char* CCTPQuoteApi::GetTradingDay()
{
	if (m_pThostMdApi == NULL || m_nSessionID == 0) return "";
	return m_pThostMdApi->GetTradingDay();
}