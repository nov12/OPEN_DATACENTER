#include "Core.h"

/*
	To test the library, include "Core.h" from an application project
	and call CoreTest().
	
	Do not forget to add the library to Project Dependencies in Visual Studio.
*/

static int s_Test = 0;

extern "C" int CoreTest();

int CoreTest()
{
	return ++s_Test;
}