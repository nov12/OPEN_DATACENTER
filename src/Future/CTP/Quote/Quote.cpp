#include "Quote.h"

CFAQuote::CFAQuote(IQuoteEvent*pEvent) :m_pEvent(pEvent)
{
	m_quoteSpi.SetIQutoe(pEvent);
	return;
}

int CFAQuote::Connect(char *address)
{
	return  true == m_quoteApi.Init(&m_quoteSpi, address) ? SUCCESS : FAIL;
}
int CFAQuote::Login(char*broker, char *account, char*pwd)
{
	return true == m_quoteApi.Login(broker, account, pwd);
}
int CFAQuote::Logout()
{
	return 0;
}
int CFAQuote::DisConnect()
{
	m_quoteApi.Disconnect();
	return 0;
}

//
//int  CFAQuote::SubIns(vIns &ins)
//{
//	int nCount = ins.size();
//	char *inss[INS_MAX_COUNT];
//	for (size_t i = 0; i < nCount; i++)
//	{
//		inss[i] = new char[INSTRUMENT1_ID_LENGTH];
//		sprintf_s(inss[i], INSTRUMENT1_ID_LENGTH, ins[i].c_str());
//	}
//	//m_quoteApi.SubMarketData(inss, ins.size());
//	//return SUCCESS;
//
//	return SubIns(inss, nCount);
//}

int  CFAQuote::SubIns(char*szIns[], const int nCont)
{
	m_quoteApi.SubMarketData(szIns, nCont);
	return SUCCESS;
}

/*

*/

extern "C" IFAQuote* CreateObject(IQuoteEvent*pEvent);
extern "C" int ReleaseObject(IFAQuote * pQuote);

IFAQuote* CreateObject(IQuoteEvent*pEvent)
{

	CFAQuote * _FAQuote = NULL;

	if (!_FAQuote)
	{
		_FAQuote = new CFAQuote(pEvent);
	}

	return _FAQuote;
}
int ReleaseObject(IFAQuote * pQuote)
{
	if (pQuote)
	{
		pQuote->DisConnect();
		delete pQuote;
		pQuote = NULL;
	}

	return 0;
}

/*
	To test the library, include "Quote.h" from an application project
	and call QuoteTest().
	
	Do not forget to add the library to Project Dependencies in Visual Studio.
*/

static int s_Test = 0;

extern "C" int QuoteTest();

int QuoteTest()
{
	return ++s_Test;
}