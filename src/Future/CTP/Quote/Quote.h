#pragma once

#include "Quote/FAQuote_Def.h"
#include "ctpImpl/CTPQuoteApi.h"
#include "ctpImpl/CTPQuoteSpi.h"

class CFAQuote :public IFAQuote{
public:
	CFAQuote(void) :m_pEvent(0){}
	explicit  CFAQuote(IQuoteEvent*pEvent);
	// TODO:  ÔÚ´ËÌí¼ÓÄúµÄ·½·¨¡£

	virtual int Connect(char *address);
	virtual int Login(char*broker, char *account, char*pwd);
	virtual int Logout();
	virtual int DisConnect();
	//virtual int SubIns(vIns &ins);
	virtual int SubIns(char*szIns[], const int nCont);
	virtual int UnSubIns(vIns &ins) { return 0; }
public:

	CCTPQuoteSpi	m_quoteSpi;
	CCTPQuoteApi	m_quoteApi;
	IQuoteEvent		*m_pEvent;
};

#ifdef __cplusplus
extern "C" {
#endif

int QuoteTest();
IFAQuote* CreateObject(IQuoteEvent*pEvent);
int ReleaseObject(IFAQuote * pQuote);

#ifdef __cplusplus
}
#endif
