#pragma once
#include "AutoDLL.h"
#include "Quote/FAQuote_Def.h"

class CYKQuote:public IQuoteEvent
{
public:
	CYKQuote();
	~CYKQuote();
	//////////////////////////////////////////////////////////////////////////
public:
	// 行情连接处理
	virtual void OnQuoteConnected();
	// 行情用户登录成功
	virtual void OnQuoteUserLoginSuccess();
	// 行情用户登录失败
	virtual void OnQuoteUserLoginFailed(int nErrorID, const char* pszMsg);
	// 行情断开处理
	virtual void OnQuoteDisconnected(int nReason);
	// 行情错误处理
	virtual void OnQuoteError(int nErrorID, const char* pszMsg);

	// 行情订阅成功
	virtual void OnQuoteSubMarketDataSuccess(char* pszInstrumentID);
	// 行情订阅失败
	virtual void OnQuoteSubMarketDataFailed(char* pszInstrumentID, int nErrorID, const char* pszMsg);

	// 行情数据处理
	virtual void OnQuoteMarketData(tagMarketData* pstMarketData);

	// 行情退订成功
	virtual void OnQuoteUnSubMarketDataSuccess(char* pszInstrumentID);
	// 行情退订失败
	virtual void OnQuoteUnSubMarketDataFailed(char* pszInstrumentID, int nErrorID, const char* pszMsg);
};

#define INS_NAME ("ag1701")
#define URL_QUOTE ("tcp://180.168.146.187:10011")
#define BROKER_ID ("9999")
#define ACCOUNT ("025458")
#define PASSWORD ("test123")
