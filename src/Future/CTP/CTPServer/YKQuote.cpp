//#include "YKQuote.h"
#include "CTPServer.h"


CYKQuote::CYKQuote()
{
}


CYKQuote::~CYKQuote()
{
}


void CYKQuote::OnQuoteConnected()
{
	printf("%s,%d\n", __FUNCTION__, __LINE__);
	_pQuote->Login(BROKER_ID, ACCOUNT, PASSWORD);
}


void CYKQuote::OnQuoteUserLoginSuccess()
{
	printf("%s,%d\n", __FUNCTION__, __LINE__);
	char * sz[] = { INS_NAME };
	_pQuote->SubIns(sz, 1);
}
void CYKQuote::OnQuoteUserLoginFailed(int nErrorID, const char* pszMsg)
{
	printf("%s,%d\n", __FUNCTION__, __LINE__);

}
void CYKQuote::OnQuoteDisconnected(int nReason)
{
	printf("%s,%d\n", __FUNCTION__, __LINE__);

}
void CYKQuote::OnQuoteError(int nErrorID, const char* pszMsg)
{
	printf("%s,%d\n", __FUNCTION__, __LINE__);

}
void CYKQuote::OnQuoteSubMarketDataSuccess(char* pszInstrumentID)
{

}
void CYKQuote::OnQuoteSubMarketDataFailed(char* pszInstrumentID, int nErrorID, const char* pszMsg)
{

}
void CYKQuote::OnQuoteMarketData(tagMarketData* pstMarketData)
{
	printf("[%s,%d]%s,%.1f\n", __FUNCTION__, __LINE__, pstMarketData->szINSTRUMENT1ID, pstMarketData->dLastPrice);
	
	if (_pSockServer)
	{
		_pSockServer->BroadCast(0, (char*)pstMarketData, sizeof(tagMarketData));
	}
}
void CYKQuote::OnQuoteUnSubMarketDataSuccess(char* pszInstrumentID)
{

}
void CYKQuote::OnQuoteUnSubMarketDataFailed(char* pszInstrumentID, int nErrorID, const char* pszMsg)
{

}