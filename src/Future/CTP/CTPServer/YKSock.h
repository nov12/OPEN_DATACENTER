#pragma once
#include "SockServer/SockServer.h"

class YKSock:public IServerHandle
{
public:
	YKSock();
	~YKSock();
public:
	virtual void Recv(bufferevent *bev, int32_t msgID, char *pData, int nLength);
	virtual void OnDisconnect();
	virtual void OnErr();
};

