#include <string>
#include <iostream>
#include <signal.h>
#include <sys/file.h>
#include <pthread.h>
#include "CTPClient.h"

using namespace std;

IClient * _pClient = 0;

typedef struct
{
	pthread_t thread_tid;  //thread id
	long thread_count;     //connection handled
}Thread;

typedef IClient* (*YSockClientCreateObject)(char *ip, int port, IP_ADDRESS ipAddress, IClientHandle * pHandle);
typedef void(*YSockClientRun)();

#define SOCKCLIENT_SO ("libSockClient.so")
#define CREATE_API ("CreateObject")
#define RELEASE_API ("ReleaseObject")
#define RUN_API ("Run")

#define IP "127.0.0.1"
#define PORT 9999

YKClient * ykClient = new YKClient();
YSockClientCreateObject _func = 0;
YSockClientRun			_funcRun = 0;

void SockThread()
{
	YAutoDLL dll;
	_func = dll.GetFunc<YSockClientCreateObject>(SOCKCLIENT_SO, CREATE_API);
	_funcRun = dll.GetFunc<YSockClientRun>(SOCKCLIENT_SO, RUN_API);
	if (!_func)
		return;
	_pClient = _func(IP, PORT, IPV4, ykClient);
	if (!_funcRun)
		return;
	_funcRun();
}

void Release()
{

}

void *thread_main(void *arg)
{
	int index = (u_int64_t)arg;
	printf("thread [%d] starting...\n", index);

	if (0 == index)
	{
		SockThread();
	}
	else if (1 == index)
	{
	}

	return NULL;
}

int main(int argc, char *argv[])
{
		
	//Init Quote
	int32_t iThreadCount = 2;
	Thread *tptr = new Thread[iThreadCount];
	for (int32_t i = 0; i < iThreadCount; ++i)
	{
		pthread_create(&tptr[i].thread_tid, NULL, &thread_main, (void*)i);
	}

	for (int32_t i = 0; i < iThreadCount; ++i)
	{
		pthread_join(tptr[i].thread_tid, NULL);
	}

	printf("input any key to release\n");
	getchar();
	Release();
	return 0;

	return 0;
}