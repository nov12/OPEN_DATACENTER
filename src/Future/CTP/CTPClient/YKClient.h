#pragma once
#include "SockClient/SockClient.h"
#include "AutoDLL.h"
class YKClient :public IClientHandle
{
public:
	YKClient();
	~YKClient();
public:
	virtual void Recv(bufferevent *bev, int32_t msgID, char *pData, int nLength);
	virtual void OnDisconnect();
	virtual void OnErr();
};

