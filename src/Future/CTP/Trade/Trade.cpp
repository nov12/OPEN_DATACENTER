#include "Trade.h"

/*
	To test the library, include "Trade.h" from an application project
	and call TradeTest().
	
	Do not forget to add the library to Project Dependencies in Visual Studio.
*/

static int s_Test = 0;

extern "C" int TradeTest();

int TradeTest()
{
	return ++s_Test;
}