#include "SockClient/SockClient.h"
#include "SockClient/IClientImpl.h"

/*
	To test the library, include "SockClient.h" from an application project
	and call SockClientTest().
	
	Do not forget to add the library to Project Dependencies in Visual Studio.
*/

static int s_Test = 0;
char		_ip[33];
int32_t		_port = 0;
IP_ADDRESS _ipAddress = IPV4;

extern "C" int SockClientTest();
extern "C" IClient * CreateObject(char *ip, int port, IP_ADDRESS ipAddress, IClientHandle * pHandle);
extern "C" void Run();
extern "C" void ReleaseObject(IClient * pClient);

	
int SockClientTest()
{
	return ++s_Test;
}

IClient * CreateObject(char *ip, int port, IP_ADDRESS ipAddress, IClientHandle * pHandle)
{
	sprintf(_ip, "%s", ip);
	_port = port;
	_ipAddress = ipAddress;
	CLIENT->Init(pHandle);
	return CLIENT;
}

void Run()
{
	CLIENT->Connect(_ip, _port, _ipAddress);	
}

void ReleaseObject(IClient * pClient)
{
	if (pClient)
	{
		pClient->Release();
	}
}