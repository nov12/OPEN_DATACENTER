#pragma once
#include<netinet/in.h>  
#include<sys/socket.h>  
#include<unistd.h>  

#include<stdio.h>  
#include<string.h>  

#include<event.h>  
#include<event2/listener.h>  
#include<event2/bufferevent.h>  
#include<event2/thread.h>  
#include "IServer.h"

class IServerImpl:public IServer
{
public:
	explicit IServerImpl(IServerHandle *pHandle);
	~IServerImpl();
private:
	IServerImpl(const IServerImpl &impl);
	IServerImpl& operator=(const IServerImpl &);
public:
	virtual int Create(int port, IP_ADDRESS ipAddress);
	virtual int Release();
public:

	static void listener_cb(evconnlistener *listener, evutil_socket_t fd, struct sockaddr *sock, int socklen, void *arg);
	static void socket_read_cb(bufferevent *bev, void *arg);
	static void socket_event_cb(bufferevent *bev, short events, void *arg);
public:
	event_base *base;
	evconnlistener *listener;
	evconnlistener *listener6;
private:
	IServerHandle	* m_pHandle;
};
