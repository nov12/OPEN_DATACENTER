#pragma once

#define SUCCESS 0
#define FAIL 1

enum IP_ADDRESS
{
	IPV4 = 0,
	IPV6,		//兼容IPV4协议
	BOTH,		//相同端口同时监听IPV4,IPV6
};

struct HEAD
{
	int id;
	int len;
};

struct BODY
{
	char c[1];
};

struct MSG
{
	HEAD head;
	BODY body;
	
};

class IServer
{
public:
	virtual int Create(int port, IP_ADDRESS ipAddress) = 0;
	virtual int Release() = 0;
};

class IServerHandle
{
public:
	virtual void Recv(MSG *pMsg) = 0;
	virtual void Send(MSG *pMsg) = 0;
};