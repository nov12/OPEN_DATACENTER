#include "SockServer/SockServer.h"
#include "SockServer/IServerImpl.h"

/*
	To test the library, include "SockServer.h" from an application project
	and call SockServerTest().
	
	Do not forget to add the library to Project Dependencies in Visual Studio.
*/

static int s_Test = 0;
int32_t _port = 0;
IP_ADDRESS _ipAddress = IPV4;

#ifdef __cplusplus
extern "C"
{
#endif	
	int SockServerTest();
	IServer* CreateObject(int port,IP_ADDRESS ipAddress,IServerHandle * pHandle);
	void Run();
	void ReleaseObject(IServer *pServer);
#ifdef __cplusplus
}
#endif

int SockServerTest()
{
	return ++s_Test;
}

IServer* CreateObject(int port, IP_ADDRESS ipAddress, IServerHandle * pHandle)
{	
	SERVER->Init(pHandle);
//	SERVER->Create(port, ipAddress);
	_port = port;
	_ipAddress = ipAddress;
	return SERVER;
}

void Run()
{
	SERVER->Create(_port, _ipAddress);
}

void Release(IServer *pServer)
{
	if (pServer)
	{
		pServer->Release();
	}
}
