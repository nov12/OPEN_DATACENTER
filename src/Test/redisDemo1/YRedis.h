#ifndef _REDIS_H_
#define _REDIS_H_

#include <iostream>
#include <string.h>
#include <string>
#include <stdio.h>

#include <hiredis/hiredis.h>

class CYRedis
{
public:

	CYRedis();

	~CYRedis();

	bool connect(std::string host, int port, timeval& timeout);

	std::string get(std::string key);

	void set(std::string key, std::string value);

private:

	redisContext* _connect;
	redisReply* _reply;

};

#endif  //_REDIS_H_