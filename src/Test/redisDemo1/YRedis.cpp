#include "YRedis.h"

CYRedis::CYRedis(){}

CYRedis::~CYRedis()
{
	this->_connect = NULL;
	this->_reply = NULL;
}

bool CYRedis::connect(std::string host, int port, timeval& timeout)
{
	//this->_connect = redisConnect(host.c_str(), port);
	this->_connect = redisConnectWithTimeout(host.c_str(), port, timeout);
	if (this->_connect != NULL && this->_connect->err)
	{
		printf("connect error: %s\n", this->_connect->errstr);
		return 0;
	}
	return 1;
}

std::string CYRedis::get(std::string key)
{
	this->_reply = (redisReply*)redisCommand(this->_connect, "GET %s", key.c_str());
	std::string str = this->_reply->str;
	freeReplyObject(this->_reply);
	return str;
}

void CYRedis::set(std::string key, std::string value)
{
	redisCommand(this->_connect, "SET %s %s", key.c_str(), value.c_str());
}
