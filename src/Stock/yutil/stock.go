// base
package yutil

/*
	基础库
*/

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

type StockItem struct {
	index     int
	stockID   string
	stockName string
}

type FB struct {
	price  float32
	volume int
}

type Quote struct {
	fb [5]FB
}

type StockQuote struct {
	stockID string
	Quote
	time       time.Time
	lastPrice  float32
	lastVolume int
}

var FilePath string
var StockItems []StockItem = make([]StockItem, 10, 100)
var lineIndex int = -1

func processLine(line string) {

	if line == "" || lineIndex == -1 {
		lineIndex = 0
		return
	}

	sp := strings.Split(line, "\t")
	n, _ := strconv.Atoi(sp[0])
	var s StockItem
	s.index = n
	s.stockID = sp[1]
	s.stockName = sp[2]
	AddStock(s)

	fmt.Println(line)
}

func Load(filePath string) bool {
	FilePath = filePath
	fmt.Println("Load StockItems!")
	ReadLine(filePath, processLine)
	return true
}

func ReLoad() bool {
	return true
}

/*
func GetStockItems() int,[]StockItem{
	lens: = cap(StockItems)
	return lens,StockItems
}*/

func AddStock(S StockItem) {
	StockItems = append(StockItems, S)
}

func SubStock(t StockItem) {
	for k, S := range StockItems {
		if S.stockID == t.stockID {
			copy(StockItems[k:], StockItems[k+1:])
			break
		}
	}
}
