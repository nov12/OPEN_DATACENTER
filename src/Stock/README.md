#![输入图片说明](http://git.oschina.net/uploads/images/2016/1103/220628_ade15063_3843.jpeg "LOGO")
# 《开源量化平台--数据中心》

> ###QQ交流群：452113084

#golang环境配置：
>sudo tar -xzf go1.8.1.linux-xxx.tar.gz -C /usr/local
>
>sudo vi /etc/profile
>
>export PATH=$PATH:/usr/local/go/bin
>
>source /etc/profile
>
>go version
>

#go run xxx.go 编译运行
#go build xxx.go 生产二进制文件xxx

#很多文本编辑器都可以设置为保存文件时自动执行gofmt，所以你的源代码应该总是会被格式化。这里还 有一个相关的工具，goimports，会自动地添加你代码里需要用到的import声明以及需要移除的import声 明。这个工具并没有包含在标准的分发包中，然而你可以自行安装：
$ go get golang.org/x/tools/cmd/goimports

#强制类型转换
>var i int
>>var s string
>>i = strconv.Atoi(s)

#redis的go支持，选择redigo
>使用方法参考：
>>https://github.com/garyburd/redigo
>>https://godoc.org/github.com/garyburd/redigo/redis

