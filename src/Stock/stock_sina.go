// s_sina
package main

/*
	基于sina股票开放行情的DEMO
	1.广播实时行情
	2.存储redis
*/

import (
	"fmt"

	"./yutil"
)

func main() {
	fmt.Println("StockServer running...")

	yutil.Load("Table.txt")
	yutil.DBInit()
}
