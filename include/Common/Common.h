#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/timeb.h>
#include <time.h>
#include <errno.h>
#include <stdarg.h>

#include <signal.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/file.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <linux/sockios.h>
#include <net/if.h>
#include <dirent.h>
#include <dlfcn.h>
#include <sys/select.h>
#include <assert.h>

#include <stdint.h>
#include <inttypes.h>

// VS下的类型定义
#if defined(_MSC_VER)
#if defined(WIN32) || defined(_WIN32)

typedef char				                                int8_t;
typedef unsigned char		                                uint8_t;
typedef short				                                int16_t;
typedef unsigned short		                                uint16_t;
typedef int					                                int32_t;
typedef unsigned int		                                uint32_t;
typedef long long			                                int64_t;
typedef unsigned long long	                                uint64_t;

#elif defined(_WIN64)

typedef char				                                int8_t;
typedef unsigned char		                                uint8_t;
typedef short				                                int16_t;
typedef unsigned short		                                uint16_t;
typedef int					                                int32_t;
typedef unsigned int		                                uint32_t;
typedef long 				                                int64_t;
typedef unsigned long		                                uint64_t;

#endif
#endif