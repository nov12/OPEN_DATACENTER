#pragma once
#include "Common.h"

#include<event.h>  
#include<event2/listener.h>  
#include<event2/bufferevent.h>  
#include<event2/thread.h>  

#define SUCCESS 0
#define FAIL 1

enum IP_ADDRESS
{
	IPV4 = 0,
	IPV6,		//兼容IPV4协议
	BOTH,		//相同端口同时监听IPV4,IPV6
};

#pragma pack(1)

struct HEAD
{
	int32_t id;
	int32_t len;
};

template<typename T>
struct MSG
{
	HEAD	head;
	T		body;	
};


#pragma pack(0)

#define SOCK_PACKAGE				2048											//最大网络包
#define SOCK_BUFFER					(sizeof(HEAD)+SOCK_PACKAGE)

#define INPUT_BUFFER(x) bufferevent_get_input(x)
#define OUTPUT_BUFFER(x) bufferevent_get_output(x)