#pragma once

#include "ctpImpl/Quote_i.h"
#include <vector>
using namespace std;
//
////typedef char ADDRESS[URL_LEN];
////typedef char BROKERID[BROKER_ID_LENGTH];
////typedef char ACCOUNT[INVESTOR_ID_LENGTH];
////typedef char PWD[PASSWORD_LENGTH];
////typedef char INSTRUMENT[INSTRUMENT1_ID_LENGTH];

typedef std::string INSTRUMENT;
typedef vector<INSTRUMENT>	vIns;


class IFAQuote
{
public:
	IFAQuote(){}
	explicit IFAQuote(IQuoteEvent*pEvent){}
	virtual ~IFAQuote(){}
public:
	virtual int Connect(char *address) = 0;
	virtual int Login(char*broker,char *account,char*pwd) = 0;
	virtual int Logout() = 0;
	virtual int DisConnect() = 0;
	//virtual int SubIns(vIns &ins) = 0;
	virtual int SubIns(char*szIns[],const int nCont) = 0;
	virtual int UnSubIns(vIns &ins) = 0;


protected:
	IFAQuote(const IFAQuote&);
	IFAQuote& operator=(const IFAQuote&);

};