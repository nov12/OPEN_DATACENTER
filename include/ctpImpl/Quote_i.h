#pragma once
#include "Quote_def.h"
#include "Proxy_def.h"

class IQuoteEvent
{
public:
	// 行情连接处理
	virtual void OnQuoteConnected() = 0;
	// 行情用户登录成功
	virtual void OnQuoteUserLoginSuccess() = 0;
	// 行情用户登录失败
	virtual void OnQuoteUserLoginFailed(int nErrorID, const char* pszMsg) = 0;
	// 行情断开处理
	virtual void OnQuoteDisconnected(int nReason) = 0;
	// 行情错误处理
	virtual void OnQuoteError(int nErrorID, const char* pszMsg) = 0;

	// 行情订阅成功
	virtual void OnQuoteSubMarketDataSuccess(char* pszInstrumentID) = 0;
	// 行情订阅失败
	virtual void OnQuoteSubMarketDataFailed(char* pszInstrumentID, int nErrorID, const char* pszMsg) = 0;

	// 行情数据处理
	virtual void OnQuoteMarketData(tagMarketData* pstMarketData) = 0;

	// 行情退订成功
	virtual void OnQuoteUnSubMarketDataSuccess(char* pszInstrumentID) = 0;
	// 行情退订失败
	virtual void OnQuoteUnSubMarketDataFailed(char* pszInstrumentID, int nErrorID, const char* pszMsg) = 0;
};



// 行情报价接口
class IQuote
{
public:
	// 初始化
	virtual bool Init(CThostFtdcMdSpi* pThostMdSpi, char* FrontAddr) = 0;
	// 清理
	virtual void Fini() = 0;
	// 登录
	virtual bool Login(char* pszBroker, char* pszInvestor, char* pszPassword) = 0;
	// 断开前置
	virtual void Disconnect() = 0;
	// 订阅行情
	virtual bool SubMarketData(char* pszInstrumentID[], int nCount) = 0;
	// 退订行情
	virtual bool UnSubMarketData(char* pszInstrumentID[], int nCount) = 0;
	// 获取当前交易日
	virtual const char* GetTradingDay() = 0;
};