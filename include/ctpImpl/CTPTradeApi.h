#include "ctp/ThostFtdcTraderApi.h"
#include "Trade_i.h"


class CCTPTradeApi: public ITrade
{
public:
	CCTPTradeApi(void);
	~CCTPTradeApi(void);

private:
	static int                              m_nRequestSeq;                          // 请求序列

private:
	CThostFtdcTraderApi*	                m_pTradeApi;                            // 交易API接口

private:
	int                                     m_nSessionID;                           // 登录成功后分配到的Session
	int										m_nFrontID;								//前置ID

	std::string                             m_szBrokerID;                           // 经纪ID
	std::string                             m_szInvestorID;                         // 投资人ID


	//辅助函数
public:
	void	SetSessionID(int nSessionID, int nFrontID, int nOrderRef){m_nSessionID = nSessionID; m_nFrontID = nFrontID; m_nRequestSeq = nOrderRef;}

	// ITrade impl - 基础接口
public:
	// 初始化
	virtual bool Init(CThostFtdcTraderSpi* pThostTradeSpi, char* FrontAddr);
	// 清理
	virtual void Fini();


	// 登录
	virtual bool Login(const char* pszBroker, const char* pszInvestor, const char* pszPassword);
	// 断开前置
	virtual void Disconnect();

	// 获取当前交易日
	virtual const char* GetTradingDay();

	// ITrade impl - 结算单接口
public:
	// 确认结算单
	virtual bool ConfirmSettlement();
	// 查询结算单确认情况
	virtual bool QuerySettlementConfirm();
	// 查询结算单(不提供交易日,则默认为上一交易日)
	virtual bool QuerySettlement(const char* pszTradingDay=NULL);

	// ITrade impl - 账户接口
public:
	// 修改账户密码
	virtual bool UpdateUserPassword(const char* pszOldPassword, const char* pszNewPassword);
	// 修改资金账户密码
	virtual bool UpdateTradingAccountPassword(const char* pszOldPassword, const char* pszNewPassword);
	// 查询资金账户
	virtual bool QueryTradingAccount();

	// ITrade impl - 持仓接口
public:
	// 查询持仓
	virtual bool QueryPosition();
	// 查询持仓明细
	virtual bool QueryPositionDetail(const char* pszInstrumentID);

	// ITrade impl - 报单接口
public:
	// 报单录入
	virtual bool OrderInsert(const char* pszInstrumentID, enTradeType nTradeType, enTradeDir nTradeDir, enTradeOperate nTradeOperate, enTradeOrderType nOrderType, double dPrice, int nVolume, char* pszOrderRefSuffix);
	// 报单操作
	virtual bool OrderAction(stOrderInfo* pOrder);
	// 查询报单
	virtual bool QueryOrder();
	// 查询成交
	virtual bool QueryTrade();





	// ITrade impl - 基础查询
public:
	// 查询交易所
	virtual bool QueryExchange(const char* pszExchangeID);
	// 查询合约
	virtual bool QueryInstrument(const char* pszInstrumentID);
	// 查询商品
	virtual bool QueryProduct(const char* pszProductID, enProductType nProductType); 

	// ITrade impl - 预埋单接口
public:
	// 预埋单录入
	virtual bool ParkedOrderInsert();
	// 预埋单操作
	virtual bool ParketOrderAction();
	// 删除预埋单
	virtual bool RemoveParketOrder();
	// 删除预埋单操作
	virtual bool RemoveParkedOrderAction();
	// 查询预埋单
	virtual bool QueryParkedOrder(const char* pszExchangeID, const char* pszInstrumentID);
	// 查询预埋单操作
	virtual bool QueryParkedOrderAction(const char* pszExchangeID, const char* pszInstrumentID);

	// ITrade impl - 银期接口
public:
	// 银行资金转期货
	virtual bool TransferFromBankToFuture();
	// 期货资金转银行
	virtual bool TransferFromFutureToBank();

	// 查询转账银行
	virtual bool QueryTransferBank();
	// 查询转账流水
	virtual bool QueryTransferStatement();
	// 查询银期签约关系
	virtual bool QueryBankFutureContract();
	// 查询签约银行
	virtual bool QueryContractBank();
	// 查询银行余额
	virtual bool QueryBankRemainingBalance();
};