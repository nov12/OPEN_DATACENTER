#pragma once
#include <stdio.h>
#include "IClient.h"

#ifdef __cplusplus
extern "C" {
#endif

int SockClientTest();
IClient * CreateObject(char *ip,int port,IP_ADDRESS ipAddress,IClientHandle * pHandle);
void Run();
void ReleaseObject(IClient * pClient);

#ifdef __cplusplus
}
#endif

extern char		_ip[33];
extern int32_t _port;
extern IP_ADDRESS _ipAddress;