#pragma once
#include <stdio.h>
#include <string>
#include "IServer.h"

#ifdef __cplusplus
extern "C" {
#endif

int SockServerTest();
IServer* CreateObject(int port,IP_ADDRESS ipAddress,IServerHandle * pHandle);
void Run();
void ReleaseObject(IServer *pServer);

#ifdef __cplusplus
}
#endif

extern int32_t _port;
extern IP_ADDRESS _ipAddress;