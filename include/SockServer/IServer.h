#pragma once
#include "Common/SockCommon.h"

class IServerHandle;
class bufferevent;

class IServer
{
public:
	virtual int Create(int port, IP_ADDRESS ipAddress) = 0;
	virtual void Init(IServerHandle * pHandle) = 0;
	virtual int Release() = 0;
	virtual void Send(bufferevent *bev, int32_t msgID, char *pData, int nLength) = 0;
	virtual void BroadCast(int32_t msgID, char *pData, int nLength) = 0;
};

class IServerHandle
{
public:
	virtual void Recv(bufferevent *bev, int32_t msgID, char *pData, int nLength) = 0;
	virtual void OnDisconnect() = 0;
	virtual void OnErr() = 0;
};