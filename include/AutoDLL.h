#pragma once
#include <stdio.h>
#include <dlfcn.h>
#include <unistd.h>

class YAutoDLL
{
public:
	YAutoDLL() :handle(0)
	{
	}

	~YAutoDLL()
	{
		if (handle)
		{
			dlclose(handle);
		}
		handle = 0;
	}
public:
	template<typename Proc>
	Proc GetFunc(char *filename, char *FuncName)
	{
		if (!handle)
			handle = dlopen(filename, RTLD_LAZY);
		if (!handle){
			printf("Error: %s\n", dlerror());
			return 0;
		}
		char * error = 0;	
		Proc pfnProc = NULL;
		pfnProc = (Proc)dlsym(handle, FuncName);
		if ((error = dlerror()) != NULL){
			printf("Error: dlsym[%s]\n", error);
			return 0;
		}

		return pfnProc;
	}
private:
	void *handle;
};

